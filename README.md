# mta-hosting-optimizer

A service that exposes IP configuration data. 

## Prerequisites
- [Node](https://nodejs.org/en/download/)

## Run

To run the application locally run:
```
docker-compose --env-file ./env-mta up
```

The application will be available at http://localhost:4001 with the following endpoint: http://localhost:4001/api/hostname

## Test

Unit test is the part of Deployment, so I haved added two stages in CI/CD. First stage will perform unit test and other stage will perform Deployment.
